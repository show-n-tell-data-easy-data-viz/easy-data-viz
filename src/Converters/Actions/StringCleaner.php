<?php

/**
 * 
 */
class StringCleaner extends Conversion
{
	public $key = "clean_string";

	public function run( $value ) 
	{	
		if( ! is_string( $value ) ){
			// TODO: Throw exception when value doesn't match with converter?

			return $value;
		}

		return strtolower( trim ( $value ) );
	}
}
?>