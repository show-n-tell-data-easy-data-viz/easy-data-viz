<?php


class StringColumnConverter extends ColumnConverter
{
	public function __construct( $idx, $key )
	{
		parent::__construct( $idx, $key );

		$this->addConversion( new StringCleaner() );
	}
}

?>