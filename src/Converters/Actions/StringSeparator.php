<?php
/**
 * 
 */
class StringSeparator extends Conversion
{
	public $key = "separate_string";

	private $separator;

	public function __construct( String $separator = ","){
		$this->separator = $separator;
	}

	public function run( $value )
	{
		if (! is_string( $value ) ){
			// TODO: Throw exception when value doesn't match with converter?

			return $value;
		}

		$values = explode( $this->separator, $value );

		return array_map( 'trim', $values );
	}
}
?>