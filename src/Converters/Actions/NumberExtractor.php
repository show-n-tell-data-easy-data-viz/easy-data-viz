<?php

/**
 * 
 */
class NumberExtractor extends Conversion
{
	public $key = "extract_number";

	public function run( $value )
	{
		if( ! is_string( $value ) ){
			// TODO: Throw exception when value doesn't match with converter?

			return $value;
		}
		
		$result;
		
		preg_match( "/\d+/", $value, $result );

		if( is_array( $result ) && count( $result ) > 0 ){
			return $result[ 0 ];
		}
		return NULL;
	}
}

?>