<?php
class ColumnConverter
{
	public $position;
	public $key;
	private $conversions = Array();

	public function __construct( $idx, $key )
	{
		$this->position = $idx;
		$this->key = $key;

		return $this;
	}

	public function addConversion( Conversion $conversion ) : ColumnConverter 
	{
		if( !key_exists( $conversion->key, $this->conversions ) ) {
			$this->conversions[ $conversion->key ] = $conversion;
		}

		return $this;
	}

	public function getConversion( String $key )
	{
		if( key_exists( $key, $this->conversions ) )
		{
			return $this->conversions[ $key ];
		}
		
		return NULL;
	}

	public function runConversion( $value ){
		foreach( $this->conversions as $conversion ){
			$value = $conversion->run( $value );
		}
		return $value;
	}

}
?>