<?php
class RowConverter
{
	private $columns = Array();

	private $available_conversions = [
		"clean_string"		=> StringCleaner::class,
		"separate_string"	=> StringSeparator::class,
		"extract_number"	=> NumberExtractor::class
	];

	public function addColumn( ColumnConverter $column ) : void
	{
		if( key_exists( $column->key, $this->columns ) )
		{
			throw new Exception( "Cannot add the same column twice" );
		}
		
		$this->columns[ $column->key ] = $column;
	}

	public function addColumns( Array $columns ) : void
	{
		foreach($columns as $column){
			$this->addColumn( $column );
		}
	}

	/**
	 * This is just a function to open up to a simpler API
	 */
	public function setColumn( Array $column ) : void
	{
		if ( isset( $column[ "type" ] ) && $column[ "type" ] === "string" ) {
			$converter = new StringColumnConverter( $column[ "index" ], $column[ "key" ] );
		} else {
			$converter = new ColumnConverter( $column["index"], $column[ "key"] );
		}
		
		if ( ! isset( $column[ "conversions" ] ) ){

			$this->addColumn( $converter );

			return;	
		}
		
		if ( ! is_array( $column[ "conversions" ] ) ){
			throw new TypeError( "Conversions have to be in the form of an array" );
		}

		foreach( $column[ "conversions" ] as $action => $args ) {

			if( key_exists( $action, $this->available_conversions ) ){
				$conversion = new $this->available_conversions[ $action ]( ...$args );
			
				$converter->addConversion( $conversion );
			}
		}

		$this->addColumn( $converter );
	}

	public function setColumns( Array $columns ) : void
	{
		foreach( $columns as $column ){
			$this->setColumn( $column );
		}
	}

	public function __get( $property )
	{
		if( property_exists( $this, $property) )
		{
			return $this->$property;
		}

		if( key_exists( $property, $this->columns ) )
		{
			return $this->columns[$property];
		}
	}

	public function sortColumns() : void{
		uasort( $this->columns, function( $a, $b){
			if($a->position === $b->position){
				return 0;
			}
			return $a->position < $b->position ? -1 : 1;
		});
	}

	public function getColumns(){
		$this->sortColumns();

		return array_keys( $this->columns );
	}

	public function run( Array $row ) : Array
	{
		$this->sortColumns();

		$result = Array();
		
		foreach( $this->columns as $column ){
			// Check that data row index don't go out of bounds
			if( isset( $row[ $column->position ] ) ){
				$field = $row[ $column->position ]; 

				$result[ $column->key ] = $column->runConversion( $field );
			}
		}

		return $result;
	}
}


?>