<?php
class DataImporter
{
	private $row_converter;
	private $file_location;
	private $max_line_length = 2048;
	private $delimiter = ',';
	private $enclosure = '"';
	private $skip_rows = 0;

	public function __construct( $file ){
		$this->row_converter = new RowConverter();
		$this->file_location = $file;
	}

	public function setColumns( Array $column_definition ) {
		$this->row_converter->setColumns( $column_definition );

		return $this;
	}

	public function skip_n_rows( $n ){
		if( is_integer( $n ) && $n >= 0){
			$this->skip_rows = $n;
		}

		return $this;
	}

	public function import( $callback = NULL )
	{
		if( ($handle = fopen( $this->file_location, "r" ) ) !== FALSE )
		{
			$row_index = 1;

			while( ( $datarow = fgetcsv( 
				$handle, 
				$this->max_line_length, 
				$this->delimiter, 
				$this->enclosure) ) !== FALSE
			) {
				if( is_array( $datarow) && count( $datarow ) > 0 ){
					if( $row_index < $this->skip_rows ) {
						$row_index += 1;
						continue;
					}

					$clean_row = $this->row_converter->run( $datarow );

					if( $callback !== NULL ){
						call_user_func_array( $callback, [ $clean_row ] );
					}
				}
				$row_index += 1;
			}
		}
	}
}
?>