<?php
use PHPUnit\Framework\TestCase;


class ConversionsTest extends TestCase
{
	public function testCanCleanStrings() : void
	{
		$converter = new StringCleaner();

		$this->assertEquals(
			"test_string",
			$converter->run("Test_STring")
		);
	}

	public function testCanExtractNumberFromString(): void
	{
		$converter = new NumberExtractor();

		$this->assertEquals(
			"15",
			$converter->run("15. this is a test")
		);
	}

	public function testCanExtractsOnlyFirstNumberFromString(): void
	{
		$converter = new NumberExtractor();

		$this->assertEquals(
			"15",
			$converter->run("15. this is a test, 42")
		);
	}

	public function testCanSeparateStrings() : void
	{
		$converter = new StringSeparator( "+" );

		$this->assertEquals(
			["hello", "world", "how", "are", "you"],
			$converter->run("hello+world+ how  +	are+you")
		);
	}

}

?>