<?php

use PHPUnit\Framework\TestCase;

class Store
{
	public $collection = [];

	public function add( $row )
	{
		if( ! empty( $row ) ) {
			$this->collection[] = $row;
		}
	}
}

class CsvImportTest extends TestCase
{
	public function testCanLoadSimpleFile(){
		$file_location = __DIR__ . "/testfiles/simple.csv";
		
		$cols = [
			[
				"index" => 0,
				"key" => "ID"
			],
			[
				"index" => 2,
				"key" => "secondary",
				"type" => "string"
			],
			[
				"index" => 1,
				"key" => "primary",
				"type" => "string"
			],
			[
				"index" => 3,
				"key" => "array",
				"type" => "string",
				"conversions" => [
					"separate_string" => [ "*" ]
				]
			]
		];

		

		$result = new Store;

		$importer = new DataImporter( $file_location );

		$importer->setColumns( $cols );

		$importer->import( Array( $result, "add" ) );

		$this->assertSame(
			[
				[
					"ID" => "1",
					"primary" => "test",
					"secondary" => "a simple",
					"array" => [
						"file", "with", "separators"
					]
				],
				[
					"ID" => "2",
					"primary" => "another row",
					"secondary" => "that is",
					"array" => [
						"very", "simple"
					]
				]
			],
			$result->collection
		);
	}

	/**
	 * Importer should be able to deal with empty rows or rows that are shorter than expected
	 */
	public function testCanLoadSimpleFileWithEmptyRows(){
		$file_location = __DIR__ . "/testfiles/simple_empty_rows.csv";
		
		$cols = [
			[
				"index" => 0,
				"key" => "ID"
			],
			[
				"index" => 2,
				"key" => "secondary",
				"type" => "string"
			],
			[
				"index" => 1,
				"key" => "primary",
				"type" => "string"
			],
			[
				"index" => 3,
				"key" => "array",
				"type" => "string",
				"conversions" => [
					"separate_string" => [ "*" ]
				]
			]
		];

		

		$result = new Store;

		$importer = new DataImporter( $file_location );

		$importer->setColumns( $cols );

		$importer->import( Array( $result, "add" ) );

		$this->assertSame(
			[
				[
					"ID" => "1",
					"primary" => "test",
					"secondary" => "a simple",
					"array" => [
						"file", "with", "separators"
					]
				],
				[
					"ID" => "2",
					"primary" => "another row",
				]
			],
			$result->collection
		);
	}

	/**
	 * Importer should be able to deal with skipping header row
	 */
	public function testCanLoadSkipFirstRow(){
		$file_location = __DIR__ . "/testfiles/simple.csv";
		
		$cols = [
			[
				"index" => 0,
				"key" => "ID"
			],
			[
				"index" => 2,
				"key" => "secondary",
				"type" => "string"
			],
			[
				"index" => 1,
				"key" => "primary",
				"type" => "string"
			],
			[
				"index" => 3,
				"key" => "array",
				"type" => "string",
				"conversions" => [
					"separate_string" => [ "*" ]
				]
			]
		];

		$result = new Store;

		$importer = new DataImporter( $file_location );

		$importer->setColumns( $cols )
			->skip_n_rows( 1 );

		$importer->import( Array( $result, "add" ) );

		$this->assertSame(
			[
				[
					"ID" => "1",
					"primary" => "test",
					"secondary" => "a simple",
					"array" => [
						"file", "with", "separators"
					]
				],
				[
					"ID" => "2",
					"primary" => "another row",
					"secondary" => "that is",
					"array" => [
						"very", "simple"
					]
				]
			],
			$result->collection
		);
	}

	/**
	 * Importer should be able to deal with skipping header row
	 */
	public function testCanLoadSkipNRows(){
		$file_location = __DIR__ . "/testfiles/simple.csv";
		
		$cols = [
			[
				"index" => 0,
				"key" => "ID"
			],
			[
				"index" => 2,
				"key" => "secondary",
				"type" => "string"
			],
			[
				"index" => 1,
				"key" => "primary",
				"type" => "string"
			],
			[
				"index" => 3,
				"key" => "array",
				"type" => "string",
				"conversions" => [
					"separate_string" => [ "*" ]
				]
			]
		];

		$result = new Store;

		$importer = new DataImporter( $file_location );

		$importer->setColumns( $cols )
			->skip_n_rows( 2 );

		$importer->import( Array( $result, "add" ) );

		$this->assertSame(
			[
				[
					"ID" => "2",
					"primary" => "another row",
					"secondary" => "that is",
					"array" => [
						"very", "simple"
					]
				]
			],
			$result->collection
		);
	}
}

?>