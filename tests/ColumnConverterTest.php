<?php

use PHPUnit\Framework\TestCase;


class ColumnConverterTest extends TestCase
{
	public function testConverterCanBeInstatiated() : void
	{
		$this->assertInstanceOf(
			ColumnConverter::class,
			new ColumnConverter( 0, "col")
		);
	}

	public function testCanAddConversion() : void
	{
		$converter = new ColumnConverter( 0, "col" );
		$converter->addConversion( new StringCleaner() );

		$this->assertInstanceOf(
			StringCleaner::class,
			$converter->getConversion( "clean_string" )
		);
	}

	public function testCanRunMultipleConversions() : void
	{
		$converter = new ColumnConverter(0, "col" );
		$converter->addConversion( new StringCleaner() );
		$converter->addConversion( new StringSeparator() );

		$this->assertEquals(
			["test", "the", "conversions"],
			$converter->runConversion( "Test, THE, Conversions		"),
		);
	}

	public function testColumnConverterReturnsEmptyConversion() : void
	{
		$converter = new ColumnConverter( 0, "col" );

		$this->assertEquals(
			NULL,
			$converter->getConversion( "any_key" )
		);
	}

	public function testStringColumnConverterHasStringCleaner() : void
	{
		$converter = new \StringColumnConverter( 0, "col" );

		$this->assertInstanceOf(
			StringCleaner::class,
			$converter->getConversion( "clean_string" )
		);
	}
}

?>