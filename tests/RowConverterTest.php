<?php

use PHPUnit\Framework\TestCase;

class RowConverterTest extends TestCase
{
	private $row_converter;

	protected function setUp() : void 
	{
		$this->row_converter = new RowConverter;
	}

	public function testRowConverterCanBeInstantiated() : void
	{
		$this->assertInstanceOf(
			RowConverter::class,
			$this->row_converter
		);
	}

	public function testCanAddColumnToRowConverter() : void
	{
		$column = new ColumnConverter( 0, "col" );

		$this->row_converter->addColumn( $column );

		$this->assertInstanceOf(
			ColumnConverter::class,
			$this->row_converter->col
		);

		$this->assertEquals(
			"col",
			$this->row_converter->col->key
		);
	}

	public function testCanNotAddSameColumnTwice() : void
	{
		$column = new ColumnConverter( 0, "col" );
		$duplicate = new ColumnConverter( 1, "col" );

		$this->row_converter->addColumn( $column );
		
		$this->expectException( Exception::class );

		$this->row_converter->addColumn( $duplicate );
	}

	public function testCanGetColKeysInSortedOrder() : void
	{
		$col1 = new ColumnConverter( 1, "col1");
		$col2 = new ColumnConverter( 0, "col0");

		$this->row_converter->addColumns( [$col1, $col2] );
		

		$this->assertEquals(
			["col0", "col1"],
			$this->row_converter->getColumns()
		);
	}

	/**
	 * @dataProvider rowProvider
	 */
	public function testCanConvertRows( $expected, $input): void
	{
		$string_cleaner = new StringCleaner();

		$simple = new StringColumnConverter( 0, "simple" );
		
		$array = new StringColumnConverter( 1, "array" );
		$array->addConversion( new StringSeparator( "*" ) );
		
		$start_number = new StringColumnConverter( 2, "start_number");
		$start_number->addConversion( new NumberExtractor() );
		
		$any_number = new StringColumnConverter( 3, "any_number");
		$any_number->addConversion( new NumberExtractor() );

		$this->row_converter->addColumns(
			[
				$any_number,
				$start_number,
				$array,
				$simple
			]
		);

		$this->assertSame(
			$expected,
			$this->row_converter->run( $input )
		);
	}

	/**
	 * @dataProvider rowProvider
	 */
	public function testCanSetColumnsFromArray( $expected, $input )
	{
		$cols = [
			[
				"index" 		=> 0,
				"key"			=> "simple",
				"type"			=> "string"
			],
			[
				"index" 		=> 1,
				"key"			=> "array",
				"type"			=> "string",
				"conversions"	=> [
					"separate_string" 	=> [ "*" ]
				]
			],
			[
				"index" 		=> 2,
				"key"			=> "start_number",
				"type"			=> "string",
				"conversions"	=> [
					"extract_number" 	=> []
				]
			],
			[
				"index"			=> 3,
				"key"			=> "any_number",
				"type"			=> "string",
				"conversions"	=> [
					"extract_number"	=> []
				]
			]
		];

		$this->row_converter->setColumns( $cols );

		$this->assertSame(
			$expected,
			$this->row_converter->run( $input )
		);
	}

	public function rowProvider(){
		return [
			[
				[
					"simple" => "test",
					"array" => ["some", "kind of", "separated", "values"],
					"start_number" => "5",
					"any_number" => "42"
				],
				[
					"Test",
					"Some * kind of 	* separated * VALUES",
					"5 a number",
					"Another number 42"
				],
			]
		];
	}
}

?>